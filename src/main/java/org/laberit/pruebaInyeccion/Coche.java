package org.laberit.pruebaInyeccion;

import org.springframework.beans.factory.annotation.Value;

public class Coche {
	@Value(value = "Citroen")
	private String marca;

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}
	
	public Coche() {
		setMarca("Seat");
	}
}
