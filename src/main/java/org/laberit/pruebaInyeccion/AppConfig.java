package org.laberit.pruebaInyeccion;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {

	@Bean
	public Coche coche() {
		return new Coche();
	}
	
}
