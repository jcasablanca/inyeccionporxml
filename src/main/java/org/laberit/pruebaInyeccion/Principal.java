package org.laberit.pruebaInyeccion;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Principal {

	public static void main(String[] args) {
		
		//ApplicationContext appContext = new ClassPathXmlApplicationContext("org/laberit/xml/beans.xml");
		ApplicationContext appContext = new AnnotationConfigApplicationContext(AppConfig.class);
		
		Coche cocheSpring = (Coche)appContext.getBean("coche");
		
		Coche coche = new Coche();
		
		System.out.println("La marca del cocheSpring es: "+cocheSpring.getMarca());

	}

}
